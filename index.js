const timer = document.querySelector("#sec_min");
const start_timer = document.querySelector("#start_timer");
const pause_timer = document.querySelector("#pause_timer");
const reset_timer = document.querySelector("#reset_timer");

let sec_min = 0,
  interval;

function showTime() {
  sec_min += 1;
  timer.innerHTML = toHHMMSS(sec_min);
}

function start() {
  interval = setInterval(showTime, 1000);
  runBtn([pause_timer, reset_timer, start_timer]);
}

function pause() {
  if (interval) {
    clearInterval(interval);
    interval = null;
    pause_timer.innerHTML = "Resume";
  } else {
    interval = setInterval(showTime, 1000);
    pause_timer.innerHTML = "Pause";
  }
}

function reset() {
  clearInterval(interval);
  interval = null;
  pause_timer.innerHTML = "Pause";
  sec_min = 0;
  timer.innerHTML = toHHMMSS(sec_min);
  runBtn([pause_timer, reset_timer, start_timer]);
}

function toHHMMSS(sec_min) {
  let minute = localStorage.getItem("minute");
  var minutes = Math.floor(
    (sec_min - localStorage.getItem("hour") * 3600) / 60
  );
  localStorage.setItem("minute", minutes);
  let second = localStorage.getItem("second");
  var seconds =
    sec_min -
    localStorage.getItem("hour") * 3600 -
    localStorage.getItem("minute") * 60;
  localStorage.setItem("second", seconds);

  minute = `${localStorage.getItem("minute")}`.padStart(2, "0");
  second = `${localStorage.getItem("second")}`.padStart(2, "0");

  return minute + ":" + second;
}

function runBtn(btnArr) {
  btnArr.forEach((btn) => (btn.style.display = "inline-block"));
}
